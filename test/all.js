const chai = require( 'chai' );
const { decode } = require( '../src/katapayadi' );

chai.should();

describe('katapayadi', () => {
  it('should decode malayalam properly', () => {
    decode('ശശി').should.equal('55');
    decode('ചണ്ഡാംശുചന്ദ്രാധമകുംഭിപാലൈര്‍').should.equal('31415926536');
    decode('കാക്ക').should.equal('11');
    decode('ആയുരാരോഗ്യസൌഖ്യം').should.equal('1712210');
  });
  it('should decode hindi properly', () => {
    decode(`भद्राम्बुद्धिसिद्धजन्मगणितश्रद्धा स्म यद् भूपगी`).should.equal(`314159265358979324`);
    decode(`गोपीभाग्यमधुव्रात-शृङ्गिशोदधिसन्धिग ।
    खलजीवितखाताव गलहालारसंधर ॥ `, {reverse: true}).should.equal(`31415926535897932384626433832792`);
    // decode(`गोपीभाग्यमधुव्रात-श्रुग्ङिशोदधिसन्धिग ॥
  //  खलजीवितखाताव गलहालारसंधर ॥`, {reverse: true}).should.equal('31415926535897932384626433832792');
    decode('आयुरारोग्यसौख्यम्').should.equal('1712210');
  });
  it('should decode kannada properly', () => {
    decode('ಆಯುರಾರೋಗ್ಯಸೌಖ್ಯಮ್').should.equal('1712210');
    decode(`ಗೋಪೀಭಾಗ್ಯಮಧುವ್ರಾತ-ಶೃಙ್ಗಿಶೋದಧಿಸಂಧಿಗ ||
 ಖಲಜೀವಿತಖಾತಾವ ಗಲಹಾಲಾರಸಂಧರ ||`, {reverse: true}).should.equal('31415926535897932384626433832792');
  });
  it.skip('should decode telugu properly', () => {
    decode(`గోపీభాగ్యమధువ్రాత-శృంగిశోదధిసంధిగ |
 ఖలజీవితఖాతావ గలహాలారసంధర ||`, {reverse: true}).should.equal('31415926535897932384626433832792');
  });
});
